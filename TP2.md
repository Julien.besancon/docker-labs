# TP2

## Sans network

### On arrête et efface tous les containers

``` 
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
```

### On nettoie les images:

Ici nettoyons les images non taggées déjà:

```
docker rmi $(docker images -q -f dangling=true)
```

Si vous voulez effacer une image spécifique:

```
docker rmi image-id
```

Voyons ce que ça donne:
```
docker images
```

Ce qui devrait donner quelque chose du style:

```
REPOSITORY                TAG                 IMAGE ID            CREATED             SIZE

frolvlad/alpine-python3   latest              41b105c17525        13 days ago         56.7MB
mvertes/alpine-mongo      latest              6239a416ad9e        4 weeks ago         108MB
```

### On lance mongodb

Ici 4100 = premier port disponible

```
docker run -d -p 4100:27017 -v /Docker/todos/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo
```

### On vérifie le contenu de Docker/todos

En particulier les Dockerfile et run.py de api et de app


### On build et run api et app

```
cd ~/Docker/todos/api
docker build -t roza/todos-api:v1 .
docker run -d  -p 4101:8082 -v /Docker/todos/api --name todos-api roza/todos-api:v1
cd ~/Docker/todos/app
docker build -t roza/todos-app:v1 .
docker run -d -p 4102:7777 -v /Docker/todos/app --name todos-app roza/todos-app:v1
```

Il est important de bien tagger les images pour s'y retrouver (:v1, v2, par exemple)


### Test dans un terminal

```bash
export no_proxy=servinfo-docker
```

On envoie une donnée à l'API:
```
curl -X PUT http://servinfo-docker:4101/roza -d '["un","deux","trois"]'
```

Et on la relis:
```
curl -X GET http://servinfo-docker:4101/roza
```

### Test de l'app dans un navigateur

Se connecter sur: http://servinfo-docker:4102/roza


## Avec network

### On arrête les conatiners todos

```
docker stop todos-{app,api,mongo}
docker rm todos-{app,api,mongo}
```

### On crée un réseau local dédié

```
docker network create --driver bridge todos-net
docker network ls
```

Cela va nous éviter de publier les ports de todos-mongo et todos-api et de 'sortir' du container pour les faire communiquer.

### On relance mongodb en l'attachant à todos-net

```
docker run --network todos-net -d -p 4100:27017 -v /Docker/todos/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo
```

### Dans /Docker/todos/api/run.py

```python
app.config['MONGO_HOST'] = 'todos-mongo'
app.config['MONGO_PORT'] = 27017
```

### Dans /Docker/todos/api/Dockerfile

Ajouter:

```
ENV no_proxy=todos-mongo
```

### Dans /Docker/todos/app/run.py

```python
API_URL = "http://todos-api:8082"
```

### Dans /Docker/todos/app/Dockerfile

On modifie:

```
ENV no_proxy=todos-api
```

### On rebuild  api et app

```
cd ~/Docker/todos/api
docker build -t roza/todos-api:v2 .
cd ~/Docker/todos/app
docker build -t roza/todos-app:v2 .
```

### On les relance attachées au réseau dédié

```
docker run --network todos-net -d  -p 4101:8082 -v /Docker/todos/api --name todos-api roza/todos-api:v2
docker run --network todos-net -d -p 4102:7777 -v /Docker/todos/app --name todos-app roza/todos-app:v2
```

### On teste !!!


