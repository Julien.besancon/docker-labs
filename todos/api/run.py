#! /usr/bin/env python3

from flask import Flask,  abort, request
app = Flask(__name__)

from flask_pymongo import PyMongo
app.config['MONGO_HOST'] = 'todos-mongo'
app.config['MONGO_PORT'] = 27017
mongo = PyMongo(app)

from flask_restful import Resource, Api
api = Api(app)


class Todos(Resource):
    def get(self, user):
        doc = mongo.db.users.find_one({'user': user})
        if doc is None:
            abort(404)
        return {'user': user, 'todos': doc['todos']}

    def put(self, user):
        doc = mongo.db.users.update_one(
            {'user': user},
            {'$set': {'todos': request.get_json(force=True)}},
            upsert=True)
        return {'matched_count': doc.matched_count,
                'modified_count': doc.modified_count}


api.add_resource(Todos, '/<user>')

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8082)
